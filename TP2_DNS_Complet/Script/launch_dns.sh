## Lancement des serveurs DNS

# Lancement de dwikiorg
echo -e "Lancement de dwikiorg"
himage dwikiorg killall -9 named
himage dwikiorg named -c /etc/named.conf

# Lancement de diutre
echo -e "Lancement de diutre"
himage diutre killall -9 named
himage diutre named -c /etc/named.conf

# Lancement de drtiutre
echo -e "Lancement de drtiutre"
himage drtiutre killall -9 named
himage drtiutre named -c /etc/named.conf

# Lancement de dcomza
echo -e "Lancement de dcomza"
himage dcomza killall -9 named
himage dcomza named -c /etc/named.conf

# Lancement de deduza
echo -e "Lancement de deduza"
himage deduza killall -9 named
himage deduza named -c /etc/named.conf

# Lancement de dfacebookcomza
echo -e "Lancement de dfacebookcomza"
himage dfacebookcomza killall -9 named
himage dfacebookcomza named -c /etc/named.conf

# Lancement de dre
echo -e "Lancement de dre"
himage dre killall -9 named
himage dre named -c /etc/named.conf

# Lancement de dorg
echo -e "Lancement de dorg"
himage dorg killall -9 named
himage dorg named -c /etc/named.conf

# Lancement de dza
echo -e "Lancement de dza"
himage dza killall -9 named
himage dza named -c /etc/named.conf

# Lancement de aRootServer
echo -e "Lancement de aRootServer"
himage aRootServer killall -9 named
himage aRootServer named -c /etc/named.conf

# Lancement de bRootServer
echo -e "Lancement de bRootServer"
himage bRootServer killall -9 named
himage bRootServer named -c /etc/named.conf

# Lancement de cRootServer
echo -e "Lancement de cRootServer"
himage cRootServer killall -9 named
himage cRootServer named -c /etc/named.conf

